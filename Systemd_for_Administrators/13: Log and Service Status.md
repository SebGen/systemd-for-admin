Title:       13: Log and Service Status.md
Subtitle:    Log and Service Status
Project:     Systemd
Author:      Lennart Poettering
Web:         http://0pointer.de/blog/projects/systemctl-journal.html
Date:        Fr 18 Mai 2012


Systemd for Administrators, Part XIII
=====================================

[Here's](12: Securing Your Services.md)
[the](11: Converting inetd Services.md)
[thirteenth](10: Instantiated Services.md)
[installment](09: On -etc-sysconfig and -etc-default.md)
[of](08: The New Configuration Files.md.html)
[my](07: The Blame Game.md)
[ongoing](06: Changing Roots.md)
[series](05: The Three Levels of "Off".md.html)
[on](04: Killing Services.md)
[systemd](03: How Do I Convert A SysV Init Script Into A systemd Service File.md)
[for](02: Which Service Owns Which Processes.md)
[Administrators](01: Verifying Bootup.md):


Log and Service Status
----------------------

This one is a short episode. One of the most commonly used commands on a
[systemd](http://www.freedesktop.org/wiki/Software/systemd) system is
`systemctl status` which may be used to determine the status of a service (or
other unit). It always has been a valuable tool to figure out the processes,
runtime information and other meta data of a daemon running on the system.

With Fedora 17 we introduced [the journal](http://0pointer.de/blog/projects
/the-journal.html), our new logging scheme that provides structured, indexed
and reliable logging on systemd systems, while providing a certain degree of
compatibility with classic syslog implementations. The original reason we
started to work on the journal was one specific feature idea, that to the
outsider might appear simple but without the journal is difficult and
inefficient to implement: along with the output of `systemctl status` we
wanted to show the last 10 log messages of the daemon. Log data is some of the
most essential bits of information we have on the status of a service. Hence
it it is an obvious choice to show next to the general status of the service.

And now to make it short: at the same time as we integrated the journal into
`systemd` and Fedora we also hooked up `systemctl` with it. Here's an example
output:

    
    $ systemctl status avahi-daemon.service
    avahi-daemon.service - Avahi mDNS/DNS-SD Stack
    	  Loaded: loaded (/usr/lib/systemd/system/avahi-daemon.service; enabled)
    	  Active: active (running) since Fri, 18 May 2012 12:27:37 +0200; 14s ago
    	Main PID: 8216 (avahi-daemon)
    	  Status: "avahi-daemon 0.6.30 starting up."
    	  CGroup: name=systemd:/system/avahi-daemon.service
    		  ├ 8216 avahi-daemon: running [omega.local]
    		  └ 8217 avahi-daemon: chroot helper
    
    May 18 12:27:37 omega avahi-daemon[8216]: Joining mDNS multicast group on interface eth1.IPv4 with address 172.31.0.52.
    May 18 12:27:37 omega avahi-daemon[8216]: New relevant interface eth1.IPv4 for mDNS.
    May 18 12:27:37 omega avahi-daemon[8216]: Network interface enumeration completed.
    May 18 12:27:37 omega avahi-daemon[8216]: Registering new address record for 192.168.122.1 on virbr0.IPv4.
    May 18 12:27:37 omega avahi-daemon[8216]: Registering new address record for fd00::e269:95ff:fe87:e282 on eth1.*.
    May 18 12:27:37 omega avahi-daemon[8216]: Registering new address record for 172.31.0.52 on eth1.IPv4.
    May 18 12:27:37 omega avahi-daemon[8216]: Registering HINFO record with values 'X86_64'/'LINUX'.
    May 18 12:27:38 omega avahi-daemon[8216]: Server startup complete. Host name is omega.local. Local service cookie is 3555095952.
    May 18 12:27:38 omega avahi-daemon[8216]: Service "omega" (/services/ssh.service) successfully established.
    May 18 12:27:38 omega avahi-daemon[8216]: Service "omega" (/services/sftp-ssh.service) successfully established.

This, of course, shows the status of everybody's favourite mDNS/DNS-SD daemon
with a list of its processes, along with -- as promised -- the 10 most recent
log lines. Mission accomplished!

There are a couple of switches available to alter the output slightly and
adjust it to your needs. The two most interesting switches are `-f` to enable
follow mode (as in `tail -f`) and `-n` to change the number of lines to show
(you guessed it, as in `tail -n`).

The log data shown comes from three sources: everything any of the daemon's
processes logged with libc's `syslog()` call, everything submitted using the
native Journal API, plus everything any of the daemon's processes logged to
STDOUT or STDERR. In short: everything the daemon generates as log data is
collected, properly interleaved and shown in the same format.

And that's it already for today. It's a very simple feature, but an immensely
useful one for every administrator. One of the kind "Why didn't we already do
this 15 years ago?".

Stay tuned for the next installment!


(C) Lennart Poettering.
