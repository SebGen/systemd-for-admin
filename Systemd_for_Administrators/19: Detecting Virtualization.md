Title:       Systemd for Administrators, Part XIX
Subtitle:    Detecting Virtualization
Project:     Systemd
Author:      Lennart Poettering
Web:         http://0pointer.de/blog/projects/detect-virt.html
Date:        08 Januar 2013


Systemd for Administrators, Part XIX
====================================

[Happy](18: Managing Resources.md)
[new](17: Using the Journal.md)
[year](16: Gettys on Serial Consoles (and Elsewhere).md)
[2013!](15: Watchdogs.md)
[Here](14: The Self-Explanatory Boot.md)
[is](13: Log and Service Status.md)
[now](12: Securing Your Services.md)
[the](11: Converting inetd Services.md)
[nineteenth](10: Instantiated Services.md)
[installment](09: On -etc-sysconfig and -etc-default.md)
[of](08: The New Configuration Files.md.html)
[my](07: The Blame Game.md)
[ongoing](06:_Changing_Roots.md)
[series](05: The Three Levels of "Off".md.html)
[on](04: Killing Services.md)
[systemd](03: How Do I Convert A SysV Init Script Into A systemd Service File.md)
[for](02: Which Service Owns Which Processes.md)
[Administrators](01: Verifying Bootup.md):


Detecting Virtualization
------------------------

When we started working on
[systemd](http://www.freedesktop.org/wiki/Software/systemd/) we had a closer
look on what the various existing init scripts used on Linux where actually
doing. Among other things we noticed that a number of them where checking
explicitly whether they were running in a virtualized environment (i.e. in a
kvm, VMWare, LXC guest or suchlike) or not. Some init scripts disabled
themselves in such cases[1], others enabled themselves only in such cases[2].
Frequently, it would probably have been a better idea to check for other
conditions rather than explicitly checking for virtualization, but after
looking at this from all sides we came to the conclusion that in many cases
explicitly conditionalizing services based on detected virtualization is a
valid thing to do. As a result we added a new configuration option to systemd
that can be used to conditionalize services this way: [`ConditionVirtualizatio
n`](http://www.freedesktop.org/software/systemd/man/systemd.unit.html); we
also added a small tool that can be used in shell scripts to detect
virtualization: [`systemd-detect-
virt(1)`](http://www.freedesktop.org/software/systemd/man/systemd-detect-
virt.html); and finally, we added a minimal bus interface to query this from
other applications.

Detecting whether your code is run inside a virtualized environment [is
actually not that
hard](http://cgit.freedesktop.org/systemd/systemd/tree/src/shared/virt.c#n30).
Depending on what precisely you want to detect it's little more than running
the CPUID instruction and maybe checking a few files in `/sys` and `/proc`.
The complexity is mostly about knowing the strings to look for, and keeping
this list up-to-date. Currently, the the virtualization detection code in
systemd can detect the following virtualization systems:

  * Hardware virtualization (i.e. VMs):

    * qemu
    * kvm
    * vmware
    * microsoft
    * oracle
    * xen
    * bochs
  * Same-kernel virtualization (i.e. containers):

    * chroot
    * openvz
    * lxc
    * lxc-libvirt
    * [systemd-nspawn](06: Changing Roots.md)

Let's have a look how one may make use if this functionality.


### Conditionalizing Units

Adding [`ConditionVirtualization`](http://www.freedesktop.org/software/systemd
/man/systemd.unit.html) to the `[Unit]` section of a unit file is enough to
conditionalize it depending on which virtualization is used or whether one is
used at all. Here's an example:

    
    [Unit]
    Name=My Foobar Service (runs only only on guests)
    ConditionVirtualization=yes
    
    [Service]
    ExecStart=/usr/bin/foobard

Instead of specifiying "`yes`" or "`no`" it is possible to specify the ID of a
specific virtualization solution (Example: "`kvm`", "`vmware`", ...), or
either "`container`" or "`vm`" to check whether the kernel is virtualized or
the hardware. Also, checks can be prefixed with an exclamation mark ("!") to
invert a check. For further details see the [manual
page](http://www.freedesktop.org/software/systemd/man/systemd.unit.html).


### In Shell Scripts

In shell scripts it is easy to check for virtualized systems with the
[`systemd-detect-virt(1)`](http://www.freedesktop.org/software/systemd/man
/systemd-detect-virt.html) tool. Here's an example:

    
    
    if systemd-detect-virt -q ; then
            echo "Virtualization is used:" `systemd-detect-virt`
    else
            echo "No virtualization is used."
    fi

If this tool is run it will return with an exit code of zero (success) if a
virtualization solution has been found, non-zero otherwise. It will also print
a short identifier of the used virtualization solution, which can be
suppressed with `-q`. Also, with the `-c` and `-v` parameters it is possible
to detect only kernel or only hardware virtualization environments. For
further details see the [manual
page](http://www.freedesktop.org/software/systemd/man/systemd-detect-
virt.html).


### In Programs

Whether virtualization is available is also exported on the system bus:

    
    $ gdbus call --system --dest org.freedesktop.systemd1 --object-path /org/freedesktop/systemd1 --method org.freedesktop.DBus.Properties.Get org.freedesktop.systemd1.Manager Virtualization
    (<'systemd-nspawn'>,)

This property contains the empty string if no virtualization is detected. Note
that some container environments cannot be detected directly from unprivileged
code. That's why we expose this property on the bus rather than providing a
library -- the bus implicitly solves the privilege problem quite nicely.

Note that all of this will only ever detect and return information about the
"inner-most" virtualization solution. If you stack virtualization ("We must go
deeper!") then these interfaces will expose the one the code is most directly
interfacing with. Specifically that means that if a container solution is used
inside of a VM, then only the container is generally detected and returned.


**Footonotes**

[1] For example: running certain device management service in a container
environment that has no access to any physical hardware makes little sense.

[2] For example: some VM solutions work best if certain vendor-specific
userspace components are running that connect the guest with the host in some
way.


(C) Lennart Poettering.
